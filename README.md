# Sistem Informasi Legalitas Kayu Scraper
> Repository webscraper untuk mendapatkan data **Daftar Pemegang Sertifikat LK** dari silk.dephut.go.id

### How-to
#### Clone repository 
```
$ git clone https://gitlab.com/wakataw/silk-scraper.git
$ cd silk-scraper
```

#### Install dependensi
```
$ pip3 install -r requirements.txt
```

#### Menjalankan aplikasi
```
$ python3 app.py
```
