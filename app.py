#!/usr/bin/python3

# import paket yang dibutuhkan
import random
import requests
import csv
import logging

from bs4 import BeautifulSoup

# setup untuk kepentingan proses logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
handler = logging.FileHandler('silk.log')
handler.setLevel(logging.DEBUG)
logger.addHandler(handler)


def csv_writer(row):
    """Fungsi untuk menulis data ke file csv

    Parameter:
        row : Data dalam format list
    """
    try:
        with open("data.csv", "a", newline="") as csvfile:
            writer = csv.writer(csvfile, dialect="excel",
                                delimiter="|", quotechar='"',
                                quoting=csv.QUOTE_ALL)
            writer.writerow(row)
    except Exception as e:
        logger.error(e)


def main():
    """Main Function untuk melakukan perulangan sebanyak jumlah halaman (contoh: 225)"""
    for i in range(225):
        # Deklarasi variable index halaman
        index = i + 1

        # Generate url berdasarkan XHR pada saat observasi, yaitu dengan menambahkan bilangan random antara 0 dan 1
        url = "http://silk.dephut.go.id/index.php/info/get_iuiphhk/{}".format(random.random())

        # Deklarasi variabel param untuk dikirim pada saat request
        param = {
            "tipe": 1,
            "index": index,
            "cari": "",
        }

        # mengirimkan request dengan url dan parameter yang telah digenerate dengan method POST
        # menggunakan package request
        # data disimpan kedalam variable `req`
        try:
            req = requests.post(url, data=param)
        except requests.RequestException as e:
            logger.error("index={}".format(index))
            logger.error(e)
            continue

        # proses parsing respond dari request menggunakan BeautifulSoup dengan html.parser sebagai parsernya
        # dengan terlebih dahulu melakukan proses konversi dari bytecode ke string dengan fungsi decode
        soup = BeautifulSoup(req.content.decode("utf-8"), "html.parser")

        # mencari seluruh tag <tr> pada halaman website
        tr = soup.find_all("tr")

        # looping untuk masing-masing tag <tr> dari hasil proses sebelumnya
        for each_tr in tr:
            # untuk setiap tag <tr>, dilakukan proses looping untuk setiap children (tag <td>)
            # untuk setiap tag <td>, dilakukan proses ekstraksi data menggunakan method td.text
            # dan selanjutnya ditampung pada variable row
            row = [' '.join(td.text.strip().split()) for td in each_tr.find_all("td")]

            if row:
                # menulis data hasil ekstraksi ke file menggunakan fungsi csv_writer() yang sudah dideklarasikan di awal
                csv_writer(row)

        # menampilkan log pada console pada saat proses
        logger.info("halaman {} ok ({}, index={})".format(index, url, index))


if __name__ == "__main__":
    main()
